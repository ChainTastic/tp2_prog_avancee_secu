﻿namespace Tp2_A20
{
    partial class frmAccueil
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tvPublications = new System.Windows.Forms.TreeView();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.txtContenu = new System.Windows.Forms.TextBox();
            this.gbAuthentifie = new System.Windows.Forms.GroupBox();
            this.lstResultatsFiltre = new System.Windows.Forms.ListBox();
            this.btnDeconnexion = new System.Windows.Forms.Button();
            this.btnFiltrerParCategorie = new System.Windows.Forms.Button();
            this.btnFiltrerParUtilisateur = new System.Windows.Forms.Button();
            this.txtFiltre = new System.Windows.Forms.TextBox();
            this.lblFiltre = new System.Windows.Forms.Label();
            this.btnPouceEnBas = new System.Windows.Forms.Button();
            this.btnPouceEnHaut = new System.Windows.Forms.Button();
            this.btnAjoutCommentaire = new System.Windows.Forms.Button();
            this.btnDiscussion = new System.Windows.Forms.Button();
            this.gbConnexion = new System.Windows.Forms.GroupBox();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.txtMdp = new System.Windows.Forms.TextBox();
            this.lblMotDePasse = new System.Windows.Forms.Label();
            this.lblLogin = new System.Windows.Forms.Label();
            this.btnAuthentifier = new System.Windows.Forms.Button();
            this.btnCreerCompte = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.gbAuthentifie.SuspendLayout();
            this.gbConnexion.SuspendLayout();
            this.SuspendLayout();
            // 
            // tvPublications
            // 
            this.tvPublications.Location = new System.Drawing.Point(356, 12);
            this.tvPublications.Name = "tvPublications";
            this.tvPublications.Size = new System.Drawing.Size(548, 666);
            this.tvPublications.TabIndex = 0;
            this.tvPublications.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvPublications_AfterSelect);
            // 
            // errorProvider1
            // 
            this.errorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider1.ContainerControl = this;
            // 
            // txtContenu
            // 
            this.txtContenu.Location = new System.Drawing.Point(910, 12);
            this.txtContenu.Multiline = true;
            this.txtContenu.Name = "txtContenu";
            this.txtContenu.Size = new System.Drawing.Size(498, 666);
            this.txtContenu.TabIndex = 3;
            // 
            // gbAuthentifie
            // 
            this.gbAuthentifie.Controls.Add(this.lstResultatsFiltre);
            this.gbAuthentifie.Controls.Add(this.btnDeconnexion);
            this.gbAuthentifie.Controls.Add(this.btnFiltrerParCategorie);
            this.gbAuthentifie.Controls.Add(this.btnFiltrerParUtilisateur);
            this.gbAuthentifie.Controls.Add(this.txtFiltre);
            this.gbAuthentifie.Controls.Add(this.lblFiltre);
            this.gbAuthentifie.Controls.Add(this.btnPouceEnBas);
            this.gbAuthentifie.Controls.Add(this.btnPouceEnHaut);
            this.gbAuthentifie.Controls.Add(this.btnAjoutCommentaire);
            this.gbAuthentifie.Controls.Add(this.btnDiscussion);
            this.gbAuthentifie.Location = new System.Drawing.Point(12, 12);
            this.gbAuthentifie.Name = "gbAuthentifie";
            this.gbAuthentifie.Size = new System.Drawing.Size(299, 666);
            this.gbAuthentifie.TabIndex = 2;
            this.gbAuthentifie.TabStop = false;
            this.gbAuthentifie.Text = "groupBox1";
            this.gbAuthentifie.Visible = false;
            // 
            // lstResultatsFiltre
            // 
            this.lstResultatsFiltre.FormattingEnabled = true;
            this.lstResultatsFiltre.ItemHeight = 15;
            this.lstResultatsFiltre.Location = new System.Drawing.Point(6, 326);
            this.lstResultatsFiltre.Name = "lstResultatsFiltre";
            this.lstResultatsFiltre.Size = new System.Drawing.Size(287, 304);
            this.lstResultatsFiltre.TabIndex = 10;
            this.lstResultatsFiltre.SelectedIndexChanged += new System.EventHandler(this.lstResultatsFiltre_SelectedIndexChanged);
            // 
            // btnDeconnexion
            // 
            this.btnDeconnexion.Location = new System.Drawing.Point(190, 637);
            this.btnDeconnexion.Name = "btnDeconnexion";
            this.btnDeconnexion.Size = new System.Drawing.Size(103, 23);
            this.btnDeconnexion.TabIndex = 9;
            this.btnDeconnexion.Text = "Se déconnecter";
            this.btnDeconnexion.UseVisualStyleBackColor = true;
            this.btnDeconnexion.Click += new System.EventHandler(this.btnDeconnexion_Click);
            // 
            // btnFiltrerParCategorie
            // 
            this.btnFiltrerParCategorie.Location = new System.Drawing.Point(0, 276);
            this.btnFiltrerParCategorie.Name = "btnFiltrerParCategorie";
            this.btnFiltrerParCategorie.Size = new System.Drawing.Size(269, 23);
            this.btnFiltrerParCategorie.TabIndex = 6;
            this.btnFiltrerParCategorie.Text = "Filtrer par catégorie";
            this.btnFiltrerParCategorie.UseVisualStyleBackColor = true;
            this.btnFiltrerParCategorie.Click += new System.EventHandler(this.btnFiltrerParCategorie_Click);
            // 
            // btnFiltrerParUtilisateur
            // 
            this.btnFiltrerParUtilisateur.Location = new System.Drawing.Point(0, 247);
            this.btnFiltrerParUtilisateur.Name = "btnFiltrerParUtilisateur";
            this.btnFiltrerParUtilisateur.Size = new System.Drawing.Size(269, 23);
            this.btnFiltrerParUtilisateur.TabIndex = 5;
            this.btnFiltrerParUtilisateur.Text = "Filtrer par utilisateur";
            this.btnFiltrerParUtilisateur.UseVisualStyleBackColor = true;
            this.btnFiltrerParUtilisateur.Click += new System.EventHandler(this.btnFiltrerParUtilisateur_Click);
            // 
            // txtFiltre
            // 
            this.txtFiltre.Location = new System.Drawing.Point(54, 218);
            this.txtFiltre.Name = "txtFiltre";
            this.txtFiltre.Size = new System.Drawing.Size(221, 23);
            this.txtFiltre.TabIndex = 4;
            // 
            // lblFiltre
            // 
            this.lblFiltre.AutoSize = true;
            this.lblFiltre.Location = new System.Drawing.Point(6, 221);
            this.lblFiltre.Name = "lblFiltre";
            this.lblFiltre.Size = new System.Drawing.Size(42, 15);
            this.lblFiltre.TabIndex = 5;
            this.lblFiltre.Text = "Filtre : ";
            // 
            // btnPouceEnBas
            // 
            this.btnPouceEnBas.Location = new System.Drawing.Point(6, 51);
            this.btnPouceEnBas.Name = "btnPouceEnBas";
            this.btnPouceEnBas.Size = new System.Drawing.Size(269, 23);
            this.btnPouceEnBas.TabIndex = 1;
            this.btnPouceEnBas.Text = "Pouce en bas";
            this.btnPouceEnBas.UseVisualStyleBackColor = true;
            this.btnPouceEnBas.Click += new System.EventHandler(this.btnPouceEnBas_Click);
            // 
            // btnPouceEnHaut
            // 
            this.btnPouceEnHaut.Location = new System.Drawing.Point(6, 22);
            this.btnPouceEnHaut.Name = "btnPouceEnHaut";
            this.btnPouceEnHaut.Size = new System.Drawing.Size(269, 23);
            this.btnPouceEnHaut.TabIndex = 0;
            this.btnPouceEnHaut.Text = "Pouce en haut";
            this.btnPouceEnHaut.UseVisualStyleBackColor = true;
            this.btnPouceEnHaut.Click += new System.EventHandler(this.btnPouceEnHaut_Click);
            // 
            // btnAjoutCommentaire
            // 
            this.btnAjoutCommentaire.Location = new System.Drawing.Point(6, 106);
            this.btnAjoutCommentaire.Name = "btnAjoutCommentaire";
            this.btnAjoutCommentaire.Size = new System.Drawing.Size(269, 23);
            this.btnAjoutCommentaire.TabIndex = 2;
            this.btnAjoutCommentaire.Text = "Ajouter un commentaire";
            this.btnAjoutCommentaire.UseVisualStyleBackColor = true;
            this.btnAjoutCommentaire.Click += new System.EventHandler(this.btnAjoutCommentaire_Click);
            // 
            // btnDiscussion
            // 
            this.btnDiscussion.Location = new System.Drawing.Point(6, 135);
            this.btnDiscussion.Name = "btnDiscussion";
            this.btnDiscussion.Size = new System.Drawing.Size(269, 23);
            this.btnDiscussion.TabIndex = 3;
            this.btnDiscussion.Text = "Ajouter une discussion";
            this.btnDiscussion.UseVisualStyleBackColor = true;
            this.btnDiscussion.Click += new System.EventHandler(this.btnDiscussion_Click);
            // 
            // gbConnexion
            // 
            this.gbConnexion.Controls.Add(this.txtLogin);
            this.gbConnexion.Controls.Add(this.txtMdp);
            this.gbConnexion.Controls.Add(this.lblMotDePasse);
            this.gbConnexion.Controls.Add(this.lblLogin);
            this.gbConnexion.Controls.Add(this.btnAuthentifier);
            this.gbConnexion.Controls.Add(this.btnCreerCompte);
            this.gbConnexion.Location = new System.Drawing.Point(12, 12);
            this.gbConnexion.Name = "gbConnexion";
            this.gbConnexion.Size = new System.Drawing.Size(338, 666);
            this.gbConnexion.TabIndex = 3;
            this.gbConnexion.TabStop = false;
            this.gbConnexion.Tag = "";
            this.gbConnexion.Text = "Authentification";
            // 
            // txtLogin
            // 
            this.txtLogin.Location = new System.Drawing.Point(95, 23);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(204, 23);
            this.txtLogin.TabIndex = 0;
            // 
            // txtMdp
            // 
            this.txtMdp.Location = new System.Drawing.Point(95, 52);
            this.txtMdp.Name = "txtMdp";
            this.txtMdp.PasswordChar = '*';
            this.txtMdp.Size = new System.Drawing.Size(204, 23);
            this.txtMdp.TabIndex = 1;
            // 
            // lblMotDePasse
            // 
            this.lblMotDePasse.AutoSize = true;
            this.lblMotDePasse.Location = new System.Drawing.Point(6, 58);
            this.lblMotDePasse.Name = "lblMotDePasse";
            this.lblMotDePasse.Size = new System.Drawing.Size(86, 15);
            this.lblMotDePasse.TabIndex = 3;
            this.lblMotDePasse.Text = "Mot de passe : ";
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Location = new System.Drawing.Point(23, 31);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(69, 15);
            this.lblLogin.TabIndex = 2;
            this.lblLogin.Text = "Utilisateur : ";
            // 
            // btnAuthentifier
            // 
            this.btnAuthentifier.Location = new System.Drawing.Point(168, 81);
            this.btnAuthentifier.Name = "btnAuthentifier";
            this.btnAuthentifier.Size = new System.Drawing.Size(141, 23);
            this.btnAuthentifier.TabIndex = 3;
            this.btnAuthentifier.Text = "S\'authentifier";
            this.btnAuthentifier.UseVisualStyleBackColor = true;
            this.btnAuthentifier.Click += new System.EventHandler(this.btnAuthentifier_Click);
            // 
            // btnCreerCompte
            // 
            this.btnCreerCompte.Location = new System.Drawing.Point(6, 81);
            this.btnCreerCompte.Name = "btnCreerCompte";
            this.btnCreerCompte.Size = new System.Drawing.Size(141, 23);
            this.btnCreerCompte.TabIndex = 2;
            this.btnCreerCompte.Text = "Créer un compte";
            this.btnCreerCompte.UseVisualStyleBackColor = true;
            this.btnCreerCompte.Click += new System.EventHandler(this.btnCreerCompte_Click);
            // 
            // frmAccueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1442, 698);
            this.Controls.Add(this.txtContenu);
            this.Controls.Add(this.gbConnexion);
            this.Controls.Add(this.gbAuthentifie);
            this.Controls.Add(this.tvPublications);
            this.Name = "frmAccueil";
            this.Text = "Accueil";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAccueil_FormClosing);
            this.Load += new System.EventHandler(this.frmAccueil_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.gbAuthentifie.ResumeLayout(false);
            this.gbAuthentifie.PerformLayout();
            this.gbConnexion.ResumeLayout(false);
            this.gbConnexion.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView tvPublications;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.TextBox txtContenu;
        private System.Windows.Forms.GroupBox gbAuthentifie;
        private System.Windows.Forms.Button btnAjoutCommentaire;
        private System.Windows.Forms.Button btnDiscussion;
        private System.Windows.Forms.GroupBox gbConnexion;
        private System.Windows.Forms.TextBox txtLogin;
        private System.Windows.Forms.TextBox txtMdp;
        private System.Windows.Forms.Label lblMotDePasse;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Button btnAuthentifier;
        private System.Windows.Forms.Button btnCreerCompte;
        private System.Windows.Forms.TextBox txtFiltre;
        private System.Windows.Forms.Label lblFiltre;
        private System.Windows.Forms.Button btnPouceEnBas;
        private System.Windows.Forms.Button btnPouceEnHaut;
        private System.Windows.Forms.Button btnDeconnexion;
        private System.Windows.Forms.Button btnFiltrerParCategorie;
        private System.Windows.Forms.Button btnFiltrerParUtilisateur;
        private System.Windows.Forms.ListBox lstResultatsFiltre;
    }
}

