﻿using System;
using System.Windows.Forms;

namespace Tp2_A20
{
    public partial class frmAjoutDiscussion : Form
    {
        #region Attributs

        private Discussion _discussion;
        private Commentaire _commentaire;
        private readonly bool _bAjoutCommentaire;

        #endregion Attributs

        #region Accesseurs

        public Commentaire Commentaire
        {
            get { return _commentaire; }
            set { _commentaire = value; }
        }

        public Discussion Discussion
        {
            get { return _discussion; }
            set { _discussion = value; }
        }

        #endregion Accesseurs

        #region Constructeurs

        public frmAjoutDiscussion()
        {
            InitializeComponent();
            _bAjoutCommentaire = false;
        }

        public frmAjoutDiscussion(string pContenu)
        {
            InitializeComponent();
            txtTitre.Text = pContenu.Substring(0, Math.Min(50, pContenu.Length));
            txtTitre.Enabled = false;
            txtCatégorie.Enabled = false;
            _bAjoutCommentaire = true;
        }

        #endregion Constructeurs

        #region Événements

        private void btnSauvegarder_Click(object sender, EventArgs e)
        {
            if (ValiderDiscussion())
            {
                if (_bAjoutCommentaire)
                {
                    Commentaire = new Commentaire(txtContenu.Text, frmAccueil.UtilisateurConnecte.NomUtilisateur);
                }
                else
                {
                    Discussion = new Discussion(txtContenu.Text, frmAccueil.UtilisateurConnecte.NomUtilisateur,
                        txtTitre.Text, txtCatégorie.Text);
                }

                DialogResult = DialogResult.OK;
                Close();
            }
        }
        #endregion Événements

        #region Méthodes

        /// <summary>
        /// Permet de valider les champs du formulaire d'ajout de discussion/commentaire
        /// </summary>
        /// <returns>retourne vrai si tout les champs sont valides, faux autrement</returns>
        private bool ValiderDiscussion()
        {
            bool bValide = true;
            if (txtTitre.Text.Trim().Length < 3)
            {
                errorProvider1.SetError(txtTitre, "Votre titre doit comporter au moins 3 caractères");
                bValide = false;
            }

            if (!_bAjoutCommentaire && txtCatégorie.Text.Trim().Length < 3)
            {
                errorProvider1.SetError(txtCatégorie, "Votre catégorie doit comporter au moins 3 caractères");
                bValide = false;
            }

            if (txtContenu.Text.Trim().Length < 3)
            {
                errorProvider1.SetError(txtContenu, "Votre contenu doit comporter au moins 3 caractères");
                bValide = false;
            }

            return bValide;
        }

        #endregion
    }
}