﻿using System;

namespace Tp2_A20
{
    [Serializable]
    public class Utilisateur
    {
        #region Attributs

        private string _nom;
        private byte[] _mdp;

        #endregion Attributs

        #region Accesseurs

        public string NomUtilisateur
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public byte[] Mdp
        {
            get { return this._mdp; }
            set { this._mdp = value; }
        }

        #endregion Accesseurs

        #region Constructeurs

        public Utilisateur()
        {
        }

        public Utilisateur(string pNomUtilisateur, byte[] pMdp)
        {
            NomUtilisateur = pNomUtilisateur;
            this.Mdp = pMdp;
        }

        #endregion Constructeurs
    }
}