﻿using Konscious.Security.Cryptography;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Windows.Forms;

namespace Tp2_A20
{
    public static class Utilitaires
    {
        /// <summary>
        /// Permet la gestion des informations des utilisateurs
        /// </summary>
        public static class GestionUtilisateurs
        {
            /// <summary>
            /// Permet de charger les données des utilisateurs  partir d'un fichier
            /// </summary>
            /// <returns>Disctionnaire comprennant le nom de l'utilisateur ainsi
            /// que l'objet utilisateur qui lui est associé</returns>
            public static Dictionary<string, Utilisateur> ChargerDataUtilisateurs()
            {
                if (File.Exists("user.dat"))
                {
                    using (StreamReader sr = new StreamReader("user.dat"))
                    {
                        return (Dictionary<string, Utilisateur>) JsonSerializer.Deserialize(sr.ReadToEnd(),
                            typeof(Dictionary<string, Utilisateur>));
                    }
                }

                return new Dictionary<string, Utilisateur>();
            }

            /// <summary>
            /// Permet de charger les données des salts utilisateurs à partir d'un fichier
            /// </summary>
            /// <returns>Dictionnaire comprennant le nom de l'utilisateur ainsi que le salt qui lui est associé</returns>
            public static Dictionary<string, byte[]> ChargerSaltsUtilisateurs()
            {
                if (File.Exists("user.slt"))
                {
                    using (StreamReader sr = new StreamReader("user.slt"))
                    {
                        return (Dictionary<string, byte[]>) JsonSerializer.Deserialize(sr.ReadToEnd(),
                            typeof(Dictionary<string, byte[]>));
                    }
                }

                return new Dictionary<string, byte[]>();
            }

            /// <summary>
            /// Permet de sauvegarder sur des fichiers les données des utilisateurs
            /// </summary>
            /// <param name="pDicoUsers">Dictionnaire comprennant le nom de l'utilisateur et l'objet utilisateur qui lui est associé</param>
            /// <param name="pDicoSalts">Dictionnaire comprennant le nom de l'utilisateur ainsi que le salt qui lui est associé</param>
            public static void SauvegarderInfoUtilisateur(Dictionary<string, Utilisateur> pDicoUsers,
                Dictionary<string, byte[]> pDicoSalts)
            {
                using (StreamWriter sw = new StreamWriter("user.dat"))
                {
                    sw.Write(JsonSerializer.Serialize(pDicoUsers, typeof(Dictionary<string, Utilisateur>)));
                }

                using (StreamWriter sw = new StreamWriter("user.slt"))
                {
                    sw.Write(JsonSerializer.Serialize(pDicoSalts, typeof(Dictionary<string, byte[]>)));
                }
            }
        }

        /// <summary>
        /// Permet la gestion des mots de passe utilisateurs
        /// </summary>
        public static class GestionMdp
        {
            /// <summary>
            /// Permet de générer un salt de mot de passe
            /// </summary>
            /// <returns>Retourne un Salt de mot de passe</returns>
            public static byte[] SaltMotDePasse()
            {
                byte[] buffer = new byte[16];
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                rng.GetBytes(buffer);
                return buffer;
            }

            /// <summary>
            /// Permet de hasher le mot de passe combiné au salt fournis en paramètre
            /// </summary>
            /// <param name="password">Mot de passe associé à l'utilisateur</param>
            /// <param name="salt">Salt associé à l'utilisateur</param>
            /// <returns>Mot de passe hashé</returns>
            public static byte[] HashMotDePasse(string password, byte[] salt)
            {
                Argon2id argon2 = new Argon2id(Encoding.UTF8.GetBytes(password))
                {
                    Salt = salt,
                    DegreeOfParallelism = 8,
                    Iterations = 4,
                    MemorySize = 1024 * 1024
                };

                return argon2.GetBytes(16);
            }

            /// <summary>
            /// Permet de vérifier si le mot de passe correspond au mot de passe hashé
            /// </summary>
            /// <param name="password">mot de passe entré par l'utilisateur</param>
            /// <param name="salt">salt associé à l'utilisateur</param>
            /// <param name="hash">mot de pase hashé associé à l'utilisateur</param>
            /// <returns></returns>
            public static bool VerifierMdp(string password, byte[] salt, byte[] hash)
            {
                return hash.SequenceEqual(HashMotDePasse(password, salt));
            }
        }

        /// <summary>
        /// Permet la gestion des fichiers XML contenant les discussions et commentaires
        /// </summary>
        public static class GestionXml
        {
            /// <summary>
            /// Permet d'exporter au format XML les discussions et leur commentaires
            /// </summary>
            /// <param name="pPath">chemin d'accès du fichier XML</param>
            /// <param name="pDiscussions">Collection des discussions contenue dans le TreeNode</param>
            public static void ExportationXml(string pPath, TreeNodeCollection pDiscussions)
            {
                using (StreamWriter sw = new StreamWriter(pPath))
                {
                    foreach (Discussion elem in pDiscussions)
                    {
                        elem.ExporterXmlDiscussion(sw);
                    }
                }
            }
            /// <summary>
            /// Permer d'importer les discussions et les commentaires à partir du fichier xml en paramètre
            /// </summary>
            /// <param name="pPath">chemin d'accès du fichier XML</param>
            /// <returns>Liste des discussions ainsi que leurs commentaires présent dans le fichier xml</returns>
            public static List<Discussion> ImportationXml(string pPath)
            {
                Queue<string> fileDiscussion = new Queue<string>();
                List<Discussion> listeDiscussions = new List<Discussion>();
                using (StreamReader sr = new StreamReader(pPath))
                {
                    foreach (string elem in sr.ReadToEnd().Split("<"))
                    {
                        fileDiscussion.Enqueue(elem.Trim());
                    }
                }

                fileDiscussion.Dequeue();

                while (fileDiscussion.Count > 1)
                {
                    listeDiscussions.Add(ImporterDiscussion(fileDiscussion));
                }

                return listeDiscussions;
            }
            /// <summary>
            /// Permet d'importer une discussion à partir de la file de discussion en paramètre
            /// </summary>
            /// <param name="pFileDiscussion">file de chaîne de caractères représentant chaque ligne du fichier xml de discussion</param>
            /// <returns>un Objet Discussion selon le contenue de la file de chaîne de caractère reçue en paramètre</returns>
            private static Discussion ImporterDiscussion(Queue<string> pFileDiscussion)
            {
                Discussion uneDiscussion;
                pFileDiscussion.Dequeue();
                string titre = pFileDiscussion.Dequeue().Substring(6);
                pFileDiscussion.Dequeue();
                string categorie = pFileDiscussion.Dequeue().Substring(10);
                pFileDiscussion.Dequeue();
                string contenu = pFileDiscussion.Dequeue().Substring(8);
                pFileDiscussion.Dequeue();
                string auteur = pFileDiscussion.Dequeue().Substring(7);
                pFileDiscussion.Dequeue();
                uneDiscussion = new Discussion(contenu, auteur, titre, categorie);
                uneDiscussion.PouceEnHaut = Convert.ToInt32(pFileDiscussion.Dequeue().Substring(10));
                pFileDiscussion.Dequeue();
                uneDiscussion.PouceEnBas = Convert.ToInt32(pFileDiscussion.Dequeue().Substring(9));
                pFileDiscussion.Dequeue();

                while (pFileDiscussion.Peek() != "/discussion>")
                {
                    uneDiscussion.Nodes.Add(ImporterCommentaire(pFileDiscussion));
                }

                pFileDiscussion.Dequeue();
                return uneDiscussion;
            }
            /// <summary>
            /// Permet d'importer un Commentaire  à partir de la file de discussion en paramètre
            /// </summary>
            /// <param name="pFile">file de chaîne de caractères représentant chaque ligne du fichier xml de discussion</param>
            /// <returns>un object Commentaire selon le contenue de la file de chaîne de caractères reçue en paramètre</returns>
            private static Commentaire ImporterCommentaire(Queue<string> pFile)
            {
                Commentaire unCommentaire;
                pFile.Dequeue();
                string contenu = pFile.Dequeue().Substring(8);
                pFile.Dequeue();
                string auteur = pFile.Dequeue().Substring(7);
                pFile.Dequeue();
                unCommentaire = new Commentaire(contenu, auteur);
                unCommentaire.PouceEnHaut = Convert.ToInt32(pFile.Dequeue().Substring(10));
                pFile.Dequeue();
                unCommentaire.PouceEnBas = Convert.ToInt32(pFile.Dequeue().Substring(9));
                pFile.Dequeue();
                pFile.Dequeue();

                while (pFile.Peek() != "/commentaire>")
                {
                    unCommentaire.Nodes.Add(ImporterCommentaire(pFile));
                }

                return unCommentaire;
            }
        }
    }
}