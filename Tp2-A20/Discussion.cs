﻿using System.Collections.Generic;
using System.IO;

namespace Tp2_A20
{
    public class Discussion : Commentaire
    {
        #region Attributs

        private string _titre;
        private string _categorie;

        #endregion Attributs

        #region Accesseurs

        public string Categorie
        {
            get { return _categorie; }
            set { _categorie = value; }
        }

        public string Titre
        {
            get { return _titre; }
            set { _titre = value; }
        }

        #endregion Accesseurs

        #region Constructeurs

        public Discussion()
        {
        }

        public Discussion(string pContenu, string pAuteur, string pTitre, string pCategorie) : base(pContenu, pAuteur)
        {
            this.Titre = pTitre;
            this.Categorie = pCategorie;
        }

        #endregion Constructeurs

        #region Méthodes

        /// <summary>
        /// Permet d'exporter au format xml une discussion
        /// </summary>
        /// <param name="pSw">StreamWriter</param>
        public void ExporterXmlDiscussion(StreamWriter pSw)
        {
            pSw.WriteLine("<discussion>");
            pSw.WriteLine("\t<titre>" + Titre + "</titre>");
            pSw.WriteLine("\t<categorie>" + Categorie + "</categorie>");
            this.ExporterXMLInfoCommentaire(pSw);
            pSw.WriteLine("</discussion>");
        }

        /// <summary>
        /// Permet d'obtenir les commentaires d'une discussion
        /// </summary>
        /// <returns>Liste de commentaires de la discussion</returns>
        private List<Commentaire> ObtenirCommentaires()
        {
            List<Commentaire> listCommentaires = new List<Commentaire>();
            foreach (Commentaire commentaire in Nodes)
            {
                listCommentaires.Add(commentaire);
            }

            return listCommentaires;
        }

        #region Overrides of Commentaire

        /// <summary>
        /// Permet d'exporter en format xml les propriétés d'un commentaire
        /// </summary>
        /// <param name="pSw">StreamWriter</param>
        protected override void ExporterXMLInfoCommentaire(StreamWriter pSw)
        {
            pSw.WriteLine("\t<contenu>" + Contenu + "</contenu>");
            pSw.WriteLine("\t<auteur>" + Auteur + "</auteur>");
            pSw.WriteLine("\t<pouceHaut>" + PouceEnHaut + "</pouceHaut>");
            pSw.WriteLine("\t<pouceBas>" + PouceEnBas + "</pouceBas>");
        }

        #endregion

        #endregion Méthodes
    }
}