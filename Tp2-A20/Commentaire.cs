﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Tp2_A20
{
    public class Commentaire : TreeNode, IComparable<Commentaire>, IComparer
    {
        #region Attributs

        private string _contenu;
        private int _pouceEnHaut;
        private int _pouceEnBas;
        private string _auteur;

        #endregion Attributs

        #region Accesseurs

        public string Contenu
        {
            get { return _contenu; }
            set { _contenu = value; }
        }

        public int PouceEnHaut
        {
            get { return _pouceEnHaut; }
            set { _pouceEnHaut = value; }
        }

        public int PouceEnBas
        {
            get { return _pouceEnBas; }
            set { _pouceEnBas = value; }
        }

        public string Auteur
        {
            get { return _auteur; }
            set { _auteur = value; }
        }

        #endregion Accesseurs

        #region Constructeurs

        public Commentaire()
        {
        }

        public Commentaire(string pContenu, string pAuteur)
        {
            Contenu = pContenu;
            Auteur = pAuteur;
            Text = ToString();
        }

        #endregion Constructeurs

        #region Méthodes

        /// <summary>
        /// Méthode permettant d'ajouter un vote positif au commentaire
        /// </summary>
        public void AjouterPouceEnHaut()
        {
            PouceEnHaut++;
            RafraichirTexte();
        }

        /// <summary>
        /// Méthode permettant d'ajouter un vote négatif au commentaire
        /// </summary>
        public void AjouterPouceEnBas()
        {
            PouceEnBas++;
            RafraichirTexte();
        }

        /// <summary>
        /// Méthode permettant d'obtenir le ratio des votes positifs et négatif d'un commentaire
        /// </summary>
        /// <returns>Le ratio des votes positifs et négatifs d'un commentaire</returns>
        private int GetRatioPouces()
        {
            return PouceEnHaut - PouceEnBas;
        }

        /// <summary>
        /// Méthode permettant de rafraîchir le texte représentatif du commentaire
        /// </summary>
        public void RafraichirTexte()
        {
            Text = ToString();
            foreach (Commentaire sousCommentaire in Nodes)
            {
                sousCommentaire.RafraichirTexte();
            }
        }

        /// <summary>
        /// Méthode permettant de trier les commentaire contenue dans le commentaire parent
        /// </summary>
        /// <param name="pCommentaire">Commentaire parent</param>
        public void TrierCommentaires()
        {
            List<Commentaire> listeDeCommentaire = new List<Commentaire>();
            foreach (Commentaire commentaireNode in Nodes)
            {
                listeDeCommentaire.Add(commentaireNode);
            }

            listeDeCommentaire.Sort();
            Nodes.Clear();
            foreach (Commentaire commentaire in listeDeCommentaire)
            {
                Nodes.Add(commentaire);
            }
        }


        #region Overrides

        /// <summary>
        /// Permet d'obtenir une chaîne de caractère formatée contenant les 25
        /// premiers caractères du contenue, l'auteur et le ratio de votes
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string contenu = Contenu.Substring(0, Math.Min(25, Contenu.Length));
            return string.Format("{0} - {1} - {2}", contenu, Auteur, GetRatioPouces());
        }

        #region Override des méthodes de comparaisons

        /// <summary>
        /// Permet de comparer un commentaire à un autre selon son ratio de
        /// vote en ordre décroissant
        /// </summary>
        /// <param name="autreCommentaire">Autre commentaire à comparer</param>
        /// <returns>1 si plus petit, 0 si égale, -1 si plus grand</returns>
        public int CompareTo(Commentaire autreCommentaire)
        {
            if (GetRatioPouces() < autreCommentaire.GetRatioPouces()) return 1;
            if (GetRatioPouces() == autreCommentaire.GetRatioPouces()) return 0;
            return -1;
        }

        /// <summary>
        /// Permet de comparer un commentaire à un autre selon son ratio de
        /// vote en ordre décroissant
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int Compare(object x, object y)
        {
            if (x == null) throw new ArgumentNullException(nameof(x));
            if (y == null) throw new ArgumentNullException(nameof(y));
            Commentaire commentaireDeBase = x as Commentaire;
            Commentaire autreCommentaire = y as Commentaire;

            if (commentaireDeBase.GetRatioPouces() < autreCommentaire.GetRatioPouces()) return 1;
            if (commentaireDeBase.GetRatioPouces() == autreCommentaire.GetRatioPouces()) return 0;
            return -1;
        }

        #endregion Override des méthodes de comparaisons

        #endregion Overrides

        #endregion Méthodes

        /// <summary>
        /// Permet d'obtenir les commentaires correspondants au filtre et therme recherché
        /// </summary>
        /// <param name="pFiltre">Chaîne de caractères représentant le filtre choisie</param>
        /// <param name="pthermeRecherchee">Chaîne de caractères représentant le therme recherché par l'utilisateur</param>
        /// <returns>Liste des commentaires correspondants aux critères en paramètres</returns>
        public List<Commentaire> ObtenirCommentairesSelonFiltre(string pFiltre, string pthermeRecherchee)
        {
            List<Commentaire> listeCommentairesTrouves = new List<Commentaire>();
            if (pFiltre == "categorie")
            {
                Discussion discussion = ObtenirDiscussionParent(this);

                if (discussion.Categorie == pthermeRecherchee)
                {
                    listeCommentairesTrouves.Add(this);
                }
            }
            else
            {
                if (Auteur == pthermeRecherchee)
                {
                    listeCommentairesTrouves.Add(this);
                }
                List<Commentaire> commentairesRecherches;
                foreach (Commentaire sousCommentaire in Nodes)
                {
                    commentairesRecherches = sousCommentaire.ObtenirCommentairesSelonFiltre(pFiltre, pthermeRecherchee);
                    if (commentairesRecherches != null)
                    {
                        listeCommentairesTrouves.AddRange(commentairesRecherches);
                    }
                }
            }


            return listeCommentairesTrouves;
        }

        /// <summary>
        /// Permet d'obtenir le noeud racine (Discussion) du commentaire reçue en paramètre
        /// </summary>
        /// <param name="pCommentaire">Commentaire</param>
        /// <returns>noeud racine (Discussion)</returns>
        private static Discussion ObtenirDiscussionParent(Commentaire pCommentaire)
        {
            while (pCommentaire.Parent != null)
            {
                pCommentaire = (Commentaire) pCommentaire.Parent;
            }

            return (Discussion) pCommentaire;
        }

        /// <summary>
        /// Permet d'exporter en format xml un commentaire
        /// </summary>
        /// <param name="pSw">StreamWriter</param>
        public void ExporterXmlCommentaire(StreamWriter pSw)
        {
            pSw.WriteLine("\t\t<commentaire>");
            ExporterXMLInfoCommentaire(pSw);
            pSw.WriteLine("\t\t</commentaire>");
        }

        /// <summary>
        /// Permet d'exporter en format xml les propriétés d'un commentaire
        /// </summary>
        /// <param name="pSw">StreamWriter</param>
        protected virtual void ExporterXMLInfoCommentaire(StreamWriter pSw)
        {
            pSw.WriteLine("\t\t\t<contenu>" + Contenu + "</contenu>");
            pSw.WriteLine("\t\t\t<auteur>" + Auteur + "</auteur>");
            pSw.WriteLine("\t\t\t<pouceHaut>" + PouceEnHaut + "</pouceHaut>");
            pSw.WriteLine("\t\t\t<pouceBas>" + PouceEnBas + "</pouceBas>");
            foreach (Commentaire commentaire in Nodes)
            {
                commentaire.ExporterXmlCommentaire(pSw);
            }
        }
    }
}