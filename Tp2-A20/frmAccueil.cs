﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using static Tp2_A20.Utilitaires;

namespace Tp2_A20
{
    public partial class frmAccueil : Form
    {
        #region Attributs

        private Dictionary<string, Utilisateur> _dicoUsers;
        private Dictionary<string, byte[]> _dicoSalts;
        private List<Discussion> _listeDiscussions;
        private static Utilisateur _utilisateurConnecte;

        #endregion Attributs

        #region Constructeurs

        public frmAccueil()
        {
            InitializeComponent();
            tvPublications.TreeViewNodeSorter = new Commentaire();
            _dicoUsers = GestionUtilisateurs.ChargerDataUtilisateurs();
            _dicoSalts = GestionUtilisateurs.ChargerSaltsUtilisateurs();
            _listeDiscussions = GestionXml.ImportationXml("discussions.xml");
        }

        #endregion Constructeurs

        public static Utilisateur UtilisateurConnecte
        {
            get { return _utilisateurConnecte; }
            private set { _utilisateurConnecte = value; }
        }

        #region Événements

        private void frmAccueil_FormClosing(object sender, FormClosingEventArgs e)
        {
            GestionUtilisateurs.SauvegarderInfoUtilisateur(_dicoUsers, _dicoSalts);
            GestionXml.ExportationXml("discussions.xml", tvPublications.Nodes);
        }

        private void frmAccueil_Load(object sender, EventArgs e)
        {
            gbAuthentifie.Visible = false;
            gbConnexion.Visible = true;
            foreach (Discussion discussion in _listeDiscussions)
            {
                tvPublications.Nodes.Add(discussion);
            }

            RafraichirTreeView();
        }

        private void btnDiscussion_Click(object sender, EventArgs e)
        {
            frmAjoutDiscussion formulaireAjout = new frmAjoutDiscussion();
            if (formulaireAjout.ShowDialog() == DialogResult.OK)
            {
                tvPublications.Nodes.Add(formulaireAjout.Discussion);
                RafraichirTreeView();
            }
        }

        private void btnAjoutCommentaire_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (tvPublications.SelectedNode != null)
            {
                frmAjoutDiscussion formulaireAjout =
                    new frmAjoutDiscussion(((Commentaire) tvPublications.SelectedNode).Contenu);
                if (formulaireAjout.ShowDialog() == DialogResult.OK)
                {
                    tvPublications.SelectedNode.Nodes.Add(formulaireAjout.Commentaire);
                    RafraichirTreeView();
                }
            }
            else
            {
                errorProvider1.SetError(btnAjoutCommentaire, "Veuillez sélectionner un commentaire ou une discussion");
            }
        }

        private void tvPublications_AfterSelect(object sender, TreeViewEventArgs e)
        {
            RafraichirContenu((Commentaire) tvPublications.SelectedNode);
        }

        private void lstResultatsFiltre_SelectedIndexChanged(object sender, EventArgs e)
        {
            RafraichirContenu((Commentaire) lstResultatsFiltre.SelectedItem);
        }

        private void btnAuthentifier_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (ChampsAuthentificationsVides())
            {
                errorProvider1.SetError(btnAuthentifier, "Les champs utilisateurs et mot de passe sont obligatoires");
            }
            else
            {
                if (ValiderInfosUtilisateur(txtLogin.Text, txtMdp.Text))
                {
                    UtilisateurConnecte = new Utilisateur(txtLogin.Text,
                        GestionMdp.HashMotDePasse(txtMdp.Text, _dicoSalts[txtLogin.Text]));
                    gbConnexion.Visible = false;
                    gbAuthentifie.Text = string.Format("Bienvenue {0}",
                        UtilisateurConnecte.NomUtilisateur);
                    gbAuthentifie.Visible = true;
                }
                else
                {
                    errorProvider1.SetError(txtLogin, "Erreur d'authentification");
                    errorProvider1.SetError(txtMdp, "Erreur d'authentification");
                }
            }
        }


        private void btnCreerCompte_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (ChampsAuthentificationsVides())
            {
                errorProvider1.SetError(btnCreerCompte, "Les champs utilisateurs et mot de passe sont obligatoires");
            }
            else
            {
                if (_dicoUsers.ContainsKey(txtLogin.Text))
                {
                    errorProvider1.SetError(txtLogin, "Ce nom d'utilisateur est déjà pris !!");
                }
                else
                {
                    _dicoSalts.Add(txtLogin.Text, GestionMdp.SaltMotDePasse());

                    Utilisateur user = new Utilisateur(txtLogin.Text,
                        GestionMdp.HashMotDePasse(txtMdp.Text, _dicoSalts[txtLogin.Text]));
                    _dicoUsers.Add(txtLogin.Text, user);
                    MessageBox.Show("Compte créé avec succès");
                }
            }
        }

        private void btnDeconnexion_Click(object sender, EventArgs e)
        {
            UtilisateurConnecte = null;
            gbAuthentifie.Visible = false;
            gbConnexion.Visible = true;
        }

        private void btnPouceEnHaut_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (tvPublications.SelectedNode != null)
            {
                Commentaire commentaireSelectionne = (Commentaire) tvPublications.SelectedNode;
                commentaireSelectionne.AjouterPouceEnHaut();
                if (commentaireSelectionne is Discussion)
                {
                    tvPublications.Sort();
                }
                else
                {
                    ((Commentaire)commentaireSelectionne.Parent).TrierCommentaires();
                }
            }
            else
            {
                errorProvider1.SetError(btnPouceEnHaut,
                    "Veuillez sélectionner un commentaire ou une discussion");
            }
        }

        private void btnPouceEnBas_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (tvPublications.SelectedNode != null)
            {
                Commentaire commentaireSelectionne = (Commentaire) tvPublications.SelectedNode;
                commentaireSelectionne.AjouterPouceEnBas();
                if (commentaireSelectionne is Discussion)
                {
                    tvPublications.Sort();
                }
                else
                {
                    ((Commentaire)commentaireSelectionne.Parent).TrierCommentaires();
                }
            }
            else
            {
                errorProvider1.SetError(btnPouceEnBas,
                    "Veuillez sélectionner un commentaire ou une discussion");
            }
        }

        private void btnFiltrerParUtilisateur_Click(object sender, EventArgs e)
        {
            if (!ChampEstVide(txtFiltre))
            {
                RemplirResultatsFiltre("utilisateur");
            }
            else
            {
                errorProvider1.SetError(txtFiltre, "Le filtre est vide");
            }
        }

        private void btnFiltrerParCategorie_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (!ChampEstVide(txtFiltre))
            {
                RemplirResultatsFiltre("categorie");
            }
            else
            {
                errorProvider1.SetError(txtFiltre, "Le filtre est vide");
            }
        }

        #endregion Événements

        #region Méthodes

        /// <summary>
        /// Permet de vérifier si les champs de connexion/authentification sont bien remplis
        /// </summary>
        /// <returns>Vrai si au moins un des 2 est vide sinon Faux</returns>
        private bool ChampsAuthentificationsVides()
        {
            return ChampEstVide(txtLogin) || ChampEstVide(txtMdp);
        }

        /// <summary>
        /// Permet de vérifier si le texteBox reçue en paramètre contient du texte
        /// </summary>
        /// <param name="pChamp">Textebox</param>
        /// <returns>Vrai si le textebox est vide , Faux autrement</returns>
        private static bool ChampEstVide(TextBox pChamp)
        {
            return pChamp.Text == "";
        }

        /// <summary>
        /// Permet de rafraichir l'ensemble du TreeView
        /// </summary>
        private void RafraichirTreeView()
        {
            foreach (Discussion discussion in tvPublications.Nodes)
            {
                discussion.RafraichirTexte();
            }

            tvPublications.Refresh();
            tvPublications.Focus();
        }

        /// <summary>
        /// Permet de rafraichir le textbox txtContenue représentant le contenue du commentaire sélectionné
        /// </summary>
        /// <param name="pCommentaire">Commentaire selectionné</param>
        private void RafraichirContenu(Commentaire pCommentaire)
        {
            if (pCommentaire == null) throw new ArgumentNullException(nameof(pCommentaire));
            txtContenu.Text = pCommentaire.Contenu;
        }

        /// <summary>
        /// Permet de remplir la liste des commentaires et discussions selon le filtre choisie
        /// </summary>
        /// <param name="pFiltre">filtre choisie</param>
        private void RemplirResultatsFiltre(string pFiltre)
        {
            lstResultatsFiltre.Items.Clear();
            List<Commentaire> listeFiltree = new List<Commentaire>();
            foreach (Discussion discussion in tvPublications.Nodes)
            {
                listeFiltree.AddRange(discussion.ObtenirCommentairesSelonFiltre(pFiltre, txtFiltre.Text));
            }

            foreach (Commentaire commentaire in listeFiltree)
            {
                lstResultatsFiltre.Items.Add(commentaire);
            }

            RafraichirTreeView();
        }

        /// <summary>
        /// Permet de valider si les informations d'authentification de l'utilisateur sont valides
        /// </summary>
        /// <param name="pLogin">chaîne de caractères représentant le login de l'utilisateur</param>
        /// <param name="pMdp">chaîne de caractères représentant le mot de passe de l'utilisateur</param>
        /// <returns></returns>
        private bool ValiderInfosUtilisateur(string pLogin, string pMdp)
        {
            if (!_dicoUsers.ContainsKey(pLogin)) return false;
            return GestionMdp.VerifierMdp(pMdp, _dicoSalts[pLogin], _dicoUsers[pLogin].Mdp);
        }

        #endregion Méthodes
    }
}